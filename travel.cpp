/*
Output: Enter a tank size:
Input: 50
Output: Enter fuel efficiency (km per liter):
Input: 10
Output: Enter price per liter:
Input: 2.45

Output: 
We can travel 500km.  tank * 100 / eff
For every 100km it will cost 24.5lv. price * eff
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    float tank, eff, price;
    tank = eff = price = 0.;
    cout << "Enter a tank size:" <<endl;
    cin >> tank;
    cout << "Enter fuel efficiency (km per liter):" <<endl;
    cin >> eff;
    cout << "Enter price per liter:"<<endl;
    cin >> price;

    cout << setprecision(5) << "We can travel "  << tank*100/eff  << "km" <<endl;
    cout << "For every 100km it will cost " << price*eff << " lv";
    return 0;
}