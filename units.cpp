/*
Output: Enter a number in meters:
Input: 600
Output: 
600 m = 0.6 km
600 m = 6000 dm
600 m = 60000 cm */

#include <iostream>
using namespace std;

int main()
{
    int m = 0;
    cout << "Enter a number in meters: ";
    cin >> m;
    cout << m << " m = " << m / 1000.0 << " km" << endl;
    cout << m << " dm = " << m * 10 << " dm";

    return 0;
}